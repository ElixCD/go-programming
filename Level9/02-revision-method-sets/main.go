package main

import "fmt"

type persona struct {
	nombre string
}

func (p *persona) Hablar() {
	fmt.Println("Hola soy", p.nombre)
}

type humano interface {
	Hablar()
}

func DiAlgo(h humano) {
	h.Hablar()
}

func main() {
	p1 := persona{
		nombre: "Elix",
	}

	// DiAlgo(p1)
	// DiAlgo(&p1)
	p1.Hablar()

}
