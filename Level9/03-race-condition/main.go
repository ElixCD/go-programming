package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	incremento := 0
	gs := 100

	wg.Add(gs)

	for i := 0; i < gs; i++ {
		go func() {
			nueva := incremento
			runtime.Gosched()
			nueva++
			incremento = nueva
			fmt.Println("Incremento Goroutine:", incremento)
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println("Incremento:", incremento)
}
