package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	incremento := 0
	gs := 100

	wg.Add(gs)
	var m sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			m.Lock()
			nueva := incremento
			nueva++
			incremento = nueva
			fmt.Println("Incremento Goroutine:", incremento)
			m.Unlock()
			wg.Done()
		}()
	}

	wg.Wait()

	fmt.Println("Incremento:", incremento)
}
